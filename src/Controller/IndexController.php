<?php

namespace App\Controller;

use App\Entity\LeasedObject;
use App\Entity\User;
use App\Form\FiltrationForm;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Object\ObjectHandler;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword(),
                        'roles' => $user->getRoles(),
                    ];

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("ping");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage();
            }
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }


    /**
     * @Route("/", name="ping")
     * @param ApiContext $apiContext
     * @param ObjectHandler $objectHandler
     * @param Request $request
     * @return Response
     * @throws ApiException
     */
    public function indexAction(
        ApiContext $apiContext,
        ObjectHandler $objectHandler,
        Request $request
    )
    {
        $form = $this->createForm(FiltrationForm::class);
        $form->handleRequest($request);
        $objects_array = [];

        $data_objects = $form->getData();


        try {
            $objects = $apiContext->makeAllObjects();

        } catch (ApiException $e) {
            return new Response('Error: ' . $e->getMessage());
        }
//
        foreach ($objects as $object){
            $newObject = new LeasedObject();
            $objects_array[] = $objectHandler->createNewAbstractObject($newObject,$object);
        }
//
        if ($form->isSubmitted() && $form->isValid()) {
            $objects_array = [];
            $filtration = $apiContext->makeFiltrationObjects($data_objects);

            foreach ($filtration as $filter){
                $newObject = new LeasedObject();
                $objects_array[] = $objectHandler->createNewAbstractObject($newObject,$filter);
            }

        }

        return $this->render('index.html.twig',[
            'form' => $form->createView(),
            'objects' => $objects_array
        ]);
    }

    /**
     * @Route("/auth", name="auth")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     * @throws ApiException
     */
    public function authAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler,
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $error = null;

        $form = $this->createForm("App\Form\AuthType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->getByCredentials(
                $userHandler->encodePlainPassword($data['password']),
                $data['email']
            );


            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('ping');
            }

            try {
                if ($apiContext->checkClientCredentials(
                    $data['password'],
                    $data['email']
                )) {
                    $centralData = $apiContext->getClientByEmail($data['email']);

                    $user = $userHandler->createNewUser(
                        $centralData
                    );
                    $manager->persist($user);
                    $manager->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('app-client-profile');
                } else {
                    $error = 'Ты не тот, за кого себя выдаешь';
                }
            } catch (ApiException $e) {
                $error = 'Что-то где-то пошло нетак';
            }
        }

        return $this->render(
            '/sign_in.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

}
