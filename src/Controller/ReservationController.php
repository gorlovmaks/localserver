<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 06.07.18
 * Time: 14:49
 */

namespace App\Controller;


use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ReservationController extends Controller
{

    /**
     * @Route("/reservation_object", name="app_reservation")
     * @Template()
     * @param ApiContext $apiContext
     * @param Request $requestь
     * @throws ApiException
     */
    public function reservationObjects(
        ApiContext $apiContext,
        Request $request
    )
    {
        $CurrentClient = $this->getUser();

        $data = [
            'client_email'=> $CurrentClient->getEmail(),
            'object_id' => $request->request->get('object_id'),
            'room' => $request->request->get('room'),
        ];

        $apiContext->createReservation($data);
    }

}