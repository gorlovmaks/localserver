<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 01.07.18
 * Time: 21:35
 */

namespace App\Controller;


use App\Form\FiltrationForm;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ObjectController extends Controller
{

    /**
     * @Route("/registerObjectLocal", name="app_sing_up_object")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerObject(
        Request $request
    ){

        $data = '';
        $error = '';
        $form = $this->createForm('App\Form\RegisterObjectType');

        $form->handleRequest($request);

        $formData = $form->getData();
        if ($form->isSubmitted() && $form->isValid()) {

                $data = [
                    'type_object' => $formData['type_object'],
                    'NumberOfRooms' =>  $formData['NumberOfRooms'],
                    'ContactPerson' =>  $formData['ContactPerson'],
                    'price' =>  $formData['price'],
                    'coordinates' => $formData['coordinates'],
                    'phone' => $formData['phone'],
                    'adress' =>  $formData['adress'],
                    'name' => $formData['name'],
                ];

              $this->get('session')->set('form_object',$data);
              return $this->redirectToRoute('app_sing_up_object2');
        }


        return $this->render('registerObject.html.twig',array(
            'form' => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/registerObjectObject2", name="app_sing_up_object2")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws ApiException
     */
    public function registerObject2(
        Request $request,
        ApiContext $apiContext
    ){

        $data = $this->get('session')->get('form_object');

        if($data['type_object'] === 'Коттедж'){
             $form = $this->createForm('App\Form\RegisterCottage');
        }else{
            $form = $this->createForm('App\Form\RegisterPension');
        }
        $form->handleRequest($request);

        $formData = $form->getData();
        if ($form->isSubmitted() && $form->isValid()) {

            $data_merge = array_merge($data, $formData);
            $apiContext->createObject($data_merge);


            $this->get('session')->remove('form_object');
            return $this->redirectToRoute('ping');
        }


        return $this->render('registerObject2.html.twig',array(
            'form' => $form->createView()
        ));
    }
}