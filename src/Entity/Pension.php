<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 28.06.18
 * Time: 15:32
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity(repositoryClass="App\Repository\PensionRepository")
 *
 */
class Pension extends LeasedObject
{


    /**
     *
     *@var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     *
     */
    protected $dishes;

    /**
     *
     *@var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     *
     */
    protected $Mud_Vans;

    /**
     *@var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     *
     */
    protected $massage;

    /**
     * @param bool $dishes
     * @return Pension
     */
    public function setDishes(bool $dishes): Pension
    {
        $this->dishes = $dishes;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDishes(): bool
    {
        return $this->dishes;
    }

    /**
     * @param bool $Mud_Vans
     * @return Pension
     */
    public function setMudVans(bool $Mud_Vans): Pension
    {
        $this->Mud_Vans = $Mud_Vans;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMudVans(): bool
    {
        return $this->Mud_Vans;
    }

    /**
     * @param bool $massage
     * @return Pension
     */
    public function setMassage(bool $massage): Pension
    {
        $this->massage = $massage;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMassage(): bool
    {
        return $this->massage;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}