<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 06.07.18
 * Time: 13:41
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReservationRepository")
 */
class Reservation
{
    /**

     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="datetime")
     */
    private $date_from;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_to;

    /**
     *
     * @ORM\Column(type="datetime")
     */
    private $currentDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="reservation")
     *
     */
    private $tenant_id;


    /**
     *
     * @var LeasedObject
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\LeasedObject", inversedBy="reservation")
     */
    private $leasedObject;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $room;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }



    /**
     * @param string $tenant_id
     * @return Reservation
     */
    public function setTenantId(string $tenant_id): Reservation
    {
        $this->tenant_id = $tenant_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTenantId(): string
    {
        return $this->tenant_id;
    }

    /**
     * @return string
     */
    public function getLeasedObject(): string
    {
        return $this->leasedObject;
    }

    /**
     * @param string $leasedObject
     * @return Reservation
     */
    public function setLeasedObject(string $leasedObject): Reservation
    {
        $this->leasedObject = $leasedObject;
        return $this;
    }

    /**
     * @param string $room
     * @return Reservation
     */
    public function setRoom(string $room): Reservation
    {
        $this->room = $room;
        return $this;
    }

    /**
     * @return string
     */
    public function getRoom(): string
    {
        return $this->room;
    }

    /**
     * @param mixed $date_from
     * @return Reservation
     */
    public function setDateFrom($date_from)
    {
        $this->date_from = $date_from;
        return $this;
    }

    /**
     * @param mixed $date_to
     * @return Reservation
     */
    public function setDateTo($date_to)
    {
        $this->date_to = $date_to;
        return $this;
    }

    /**
     * @param mixed $currentDate
     * @return Reservation
     */
    public function setCurrentDate($currentDate)
    {
        $this->currentDate = $currentDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateFrom()
    {
        return $this->date_from;
    }

    /**
     * @return mixed
     */
    public function getDateTo()
    {
        return $this->date_to;
    }

    /**
     * @return mixed
     */
    public function getCurrentDate()
    {
        return $this->currentDate;
    }
}