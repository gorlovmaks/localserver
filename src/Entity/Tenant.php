<?php

namespace App\Entity;

use App\Model\User\UserHandler;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TenantRepository")
 */
class Tenant extends User
{
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $callName;

    /**
     * @param mixed $callName
     * @return Tenant
     */
    public function setCallName($callName)
    {
        $this->callName = $callName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCallName()
    {
        return $this->callName;
    }
}
