<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\LandlordRepository")
 */
class Landlord extends User
{
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $full_name;

    /**
     * @param mixed $full_name
     * @return Landlord
     */
    public function setFullName($full_name)
    {
        $this->full_name = $full_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->full_name;
    }
}
