<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 28.06.18
 * Time: 14:33
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LeasedObjectRepository")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"leasedobject" = "LeasedObject", "pension" = "Pension", "cottage" = "Cottage"})
 * @UniqueEntity("email")
 * @UniqueEntity("passport")
 */

class LeasedObject
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     *
     *@var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $typeObject;

    /**
     * @var Reservation
     * @ORM\Column(nullable=true)
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation", mappedBy="leasedObject")
     */
    private $reservation;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $number_of_rooms;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $contact_person;

    /**
     *@var string
     *
     * @ORM\Column(type="decimal", length=64)
     */
    protected $price;

    /**
     *@var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $adress;

    /**
     *@var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $coordinates;

    /**
     * @return string
     */
    public function getTypeObject(): string
    {
        return $this->typeObject;
    }

    /**
     * @param string $typeObject
     * @return LeasedObject
     */
    public function setTypeObject(string $typeObject)
    {
        $this->typeObject = $typeObject;
        return $this;
    }

    /**
     * @param string $name
     * @return LeasedObject
     */
    public function setName(string $name): LeasedObject
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $number_of_rooms
     * @return LeasedObject
     */
    public function setNumberOfRooms(string $number_of_rooms): LeasedObject
    {
        $this->number_of_rooms = $number_of_rooms;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumberOfRooms(): string
    {
        return $this->number_of_rooms;
    }

    /**
     * @param string $contact_person
     * @return LeasedObject
     */
    public function setContactPerson(string $contact_person): LeasedObject
    {
        $this->contact_person = $contact_person;
        return $this;
    }

    /**
     * @return string
     */
    public function getContactPerson(): string
    {
        return $this->contact_person;
    }

    /**
     * @param string $price
     * @return LeasedObject
     */
    public function setPrice(string $price): LeasedObject
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @param string $adress
     * @return LeasedObject
     */
    public function setAdress(string $adress): LeasedObject
    {
        $this->adress = $adress;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdress(): string
    {
        return $this->adress;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $coordinates
     * @return LeasedObject
     */
    public function setCoordinates(string $coordinates): LeasedObject
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     * @return string
     */
    public function getCoordinates(): string
    {
        return $this->coordinates;
    }

    /**
     * @param string $phone
     * @return LeasedObject
     */
    public function setPhone(string $phone): LeasedObject
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $reservation
     * @return LeasedObject
     */
    public function setReservation(string $reservation): LeasedObject
    {
        $this->reservation = $reservation;
        return $this;
    }

    /**
     * @return string
     */
    public function getReservation(): string
    {
        return $this->reservation;
    }

    /**
     * @param int $id
     * @return LeasedObject
     */
    public function setId(int $id): LeasedObject
    {
        $this->id = $id;
        return $this;
    }


}