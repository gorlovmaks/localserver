<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 *
 * @ORM\Entity(repositoryClass="App\Repository\LeasedObjectRepository")
 *
 */
class Cottage extends LeasedObject
{

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     *
     */
    protected $smoking_room;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     *
     */
    protected $good_attitude;



    /**
     * @param bool $good_attitude
     * @return Cottage
     */
    public function setGoodAttitude(bool $good_attitude): Cottage
    {
        $this->good_attitude = $good_attitude;
        return $this;
    }

    /**
     * @return bool
     */
    public function isGoodAttitude(): bool
    {
        return $this->good_attitude;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param bool $smoking_room
     * @return Cottage
     */
    public function setSmokingRoom(bool $smoking_room): Cottage
    {
        $this->smoking_room = $smoking_room;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSmokingRoom(): bool
    {
        return $this->smoking_room;
    }

}