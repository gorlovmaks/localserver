<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterCottage extends AbstractType
{

        public function buildForm(FormBuilderInterface $builder, array $options)
            {
                $builder

                    ->add('smoking_room',ChoiceType::class,array(
                        'choices' => array(
                            'Да' => true,
                            'Нет' => false
                        )
                    ))
                    ->add('GoodAttitude',ChoiceType::class,array(
                        'choices' => array(
                            'Да' => true,
                            'Нет' => false
                        )
                    ))
                    ->add('Далее', SubmitType::class);

            }


}

