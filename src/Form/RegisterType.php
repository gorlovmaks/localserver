<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('passport')
            ->add('password', PasswordType::class)
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Никто' => null,
                    'Аренатор' => 'tenant',
                    'Арендодатель' => 'landlord'
                ]
            ])
            ->add('save', SubmitType::class);

        /** @var User $user */
        $user = $builder->getData();

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($storedRoles) use (&$user) {
                    $currentRoles = $user->getRoles();
                    if (in_array('ROLE_TENANT', $currentRoles)) {
                        return 'tenant';
                    } elseif (in_array('ROLE_LANDLORD', $currentRoles)) {
                        return 'landlord';
                    } else {
                        return null;
                    }
                },
                function ($tagsAsString) use (&$user) {
                    $currentRoles = array_diff($user->getRoles(), [
                        'ROLE_TENANT',
                        'ROLE_LANDLORD',
                    ]);

                    switch ($tagsAsString) {
                        case 'tenant':
                            $currentRoles[] = 'ROLE_TENANT';
                            break;
                        case 'landlord':
                            $currentRoles[] = 'ROLE_LANDLORD';
                            break;
                        default:
                            $currentRoles = $user->getRoles();
                            break;
                    }

                    return $currentRoles;
                }
            ));
    }
}
