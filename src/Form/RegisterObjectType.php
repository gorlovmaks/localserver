<?php

namespace App\Form;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterObjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('type_object',ChoiceType::class,array(
                'choices' => array(
                    'Коттедж' => 'Коттедж',
                    'Пансионат' => 'Пансионат'
                ),
                'label' => 'Тип объекта'
            ))
            ->add('NumberOfRooms',NumberType::class,array(
                'label' => 'Кол-во номеров объекта',
            ))
            ->add('name',TextType::class,array(
                'label' => 'Название',
            ))
            ->add('ContactPerson')
            ->add('phone',TextType::class,array(
                'label' => 'Номер телефона',
            ))
            ->add('coordinates',TextType::class,array(
                'label' => 'Кликните мышью желаемое место на карте',
            ))
            ->add('adress',TextType::class,array(
                'label' => 'Адрес',
            ))
            ->add('price',NumberType::class,array(
                'label' => 'Цена за путевку',
            ))

            ->add('Далее', SubmitType::class);

    }
}
