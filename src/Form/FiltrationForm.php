<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FiltrationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('price_min',NumberType::class,[
                'required' => false
            ])
            ->add('price_max',NumberType::class,[
                'required' => false
            ])
            ->add('type_object',ChoiceType::class,[
                'choices' => [
                    'Пансионат' => 'Пансионат',
                    'Коттедж' => 'Коттедж'
                ],
                'required' => false
            ])
            ->add('name',TextType::class,[
                'required' => false
            ])
            ->add('Фильтровать', SubmitType::class);

    }
}
