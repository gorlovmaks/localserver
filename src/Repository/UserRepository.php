<?php

namespace App\Repository;

use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{


    public function __construct(
        RegistryInterface $registry,
        UserHandler $userHandler)
    {
        parent::__construct($registry, User::class);

    }

    /**
     * @param string $plainPassword
     * @param string $email
     * @return User|null
     */
    public function getByCredentials(string $plainPassword, string $email)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.password = :password')
                ->andWhere('a.email = :email')
                ->setParameter(
                    'password',
                    $plainPassword
                )
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getByUserOfUidSocial($network,$uid){


        $socialResult = $this->createQueryBuilder('u')
            ->select('u');
            switch ($network){
                case 'vkontakte':
                   $socialResult->where('u.vkId = :uid');
                   break;
                case 'google':
                    $socialResult->where('u.googleId = :uid');
                    break;
                case 'facebook':
                    $socialResult->where('u.faceBookId = :uid');
                    break;
                default:
                    null;
            }
        try {
            return $socialResult
                ->setParameter('uid', $uid)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
                return null;
        }
    }

    public function findOneByPassportOrEmail(string $passport, string $email) : ?User
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.passport = :passport')
                ->orWhere('a.email = :email')
                ->setParameter('passport', $passport)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
