<?php

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_FULL_OBJECTS = '/all/objects';
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_OBJECT = '/registerObject';
    const ENDPOINT_CLIENT_PASSWORD_ENCODE = '/client/password/encode';
    const ENDPOINT_CONCRETE_CLIENT_BY_EMAIL = '/client/email/{email}';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/check_client_credentials/{encodedPassword}/{email}';
    const ENDPOINT_CONCRETE_SOCIAL_CLIENT = '/client/social/{network}/{uid}';
    const ENDPOINT_CONCRETE_OBJECTS = '/filtration/objects';
    const ENDPOINT_RESERVATION = '/reservation';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makeAllObjects()
    {
        return $this->makeQuery(self::ENDPOINT_FULL_OBJECTS, self::METHOD_GET);
    }

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function makeFiltrationObjects(array $data)
    {

        return $this->makeQuery(self::ENDPOINT_CONCRETE_OBJECTS, self::METHOD_GET,$data);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
            'passport' => $passport,
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $email
     * @return array
     * @throws ApiException
     */
    public function getClientByEmail(string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param string $plainPassword
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function checkClientCredentials(string $plainPassword, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_CREDENTIALS, [
            'encodedPassword' => $this->encodePassword($plainPassword),
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param string $email
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function updateClient(string $email, array $data)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_PATCH, $data);
    }

    /**
     * @param string $password
     * @return mixed
     * @throws ApiException
     */
    public function encodePassword(string $password)
    {
        $response = $this->makeQuery(self::ENDPOINT_CLIENT_PASSWORD_ENCODE, self::METHOD_GET, [
            'plainPassword' => $password
        ]);

        return $response['result'];
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createObject($data)
    {
        return $this->makeQuery(self::ENDPOINT_OBJECT, self::METHOD_POST, $data);
    }

    /**
     * @param string $network
     * @param string $uid
     * @return bool
     * @throws ApiException
     */
    public function socialClientExist(string $network, string $uid)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_SOCIAL_CLIENT, [
            'network' => $network,
            'uid' => $uid
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $network
     * @param string $uid
     * @return bool
     * @throws ApiException
     */
    public function socialByClient(string $network, string $uid)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_SOCIAL_CLIENT, [
            'network' => $network,
            'uid' => $uid
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createReservation($data)
    {
        return $this->makeQuery(self::ENDPOINT_RESERVATION, self::METHOD_POST, $data);
    }

}
