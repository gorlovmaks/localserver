<?php


namespace App\Model\Object;


use App\Entity\Cottage;
use App\Entity\LeasedObject;
use App\Entity\Pension;

class ObjectHandler
{
    /**
     * @param array $data
     * @return LeasedObject|Cottage
     */
    public function createNewCottage(array $data) {
        $cottage = $this->createNewAbstractObject(new Cottage(), $data);
        /** @var Cottage $cottage */
        $cottage
            ->setSmokingRoom($data['smoking_room']??null)
            ->setGoodAttitude($data['GoodAttitude']??null);


        return $cottage;
    }

    /**
     * @param LeasedObject $object
     * @param array $data
     * @return LeasedObject
     */
    public function createNewAbstractObject(LeasedObject $object, array $data) {
        $object->setId($data['id']);
        $object->setPrice($data['price']);
        $object->setTypeObject($data['type_object']);
        $object->setAdress($data['adress']);
        $object->setNumberOfRooms($data['NumberOfRooms']);
        $object->setName($data['name']);
        $object->setContactPerson($data['ContactPerson']);


        return $object;
    }

    /**
     * @param array $data
     * @return LeasedObject|Pension
     */
    public function createNewPension(array $data) {
        $object = $this->createNewAbstractObject(new Pension(), $data);
        /** @var Pension $object */
        $object
            ->setMassage($data['massage']??null)
            ->setDishes($data['dishes']??null)
            ->setMudVans($data['mud_vuns']??null);

        return $object;
    }

}
